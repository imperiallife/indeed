// api-routes.js
// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});
// Import job controller
var jobController = require('./jobController');
// job routes
router.route('/jobs')
    .get(jobController.index)
    .post(jobController.new);
router.route('/jobs_search')
    .get(jobController.search);
router.route('/jobs/:job_id')
    .get(jobController.view)
    .patch(jobController.update)
    .put(jobController.update)
    .delete(jobController.delete);
// Export API routes
module.exports = router;