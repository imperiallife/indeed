// jobController.js
// Import job model
job = require('./jobModel');
// Handle index actions
exports.index = function (req, res) {
    job.get(function (err, jobs) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json(jobs);
    });
};

exports.search = function (req, res) {
    var search = ''
    if(req.query.search) {
        search = req.query.search;
    }
    job.find(
        { "text": { "$regex": search, "$options": "i" } },
        function(err,jobs) { 
            if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json(jobs);
        } 
    );
};
//Handle create job actions
exports.new = function (req, res) {
    var job = new job();
    job.url = req.body.url;
    job.slug = req.body.slug;
    job.title = req.body.title;
    job.company = req.body.company;
    job.location = req.body.location;
    job.text = req.body.text;
    job.html = req.body.html;
    job.createTime = req.body.createTime;
    job.isArchived = req.body.isArchived;

// save the job and check for errors
    job.save(function (err) {
        // if (err)
        //     res.json(err);
res.json(job);
    });
};
// Handle view job info
exports.view = function (req, res) {
    job.findById(req.params.job_id, function (err, job) {
        if (err)
            res.send(err);
        res.json(job);
    });
};
// Handle update job info
exports.update = function (req, res) {
job.findById(req.params.job_id, function (err, job) {
        if (err)
            res.send(err);
            job.url = req.body.url;
            job.slug = req.body.slug;
            job.title = req.body.title;
            job.company = req.body.company;
            job.location = req.body.location;
            job.text = req.body.text;
            job.html = req.body.html;
            job.createTime = req.body.createTime;
            job.isArchived = req.body.isArchived;
            job.tags = req.body.tags;
// save the job and check for errors
        job.save(function (err) {
            if (err)
                res.json(err);
            res.json(job);
        });
    });
};
// Handle delete job
exports.delete = function (req, res) {
    job.remove({
        _id: req.params.job_id
    }, function (err, job) {
        if (err)
            res.send(err);
res.json({
            status: "success",
            message: 'job deleted'
        });
    });
};