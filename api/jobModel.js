// jobModel.js
var mongoose = require('mongoose');
// Setup schema
var jobSchema = mongoose.Schema({
    url: String,
    slug: String,
    title: String,
    company: String,
    location: String,
    text: String,
    html: String,
    createTime: Date,
    isArchived: Boolean,
    tags: Array
});
// Export job model
var job = module.exports = mongoose.model('job', jobSchema);
module.exports.get = function (callback, limit) {
    job.find(callback).limit(limit);
}