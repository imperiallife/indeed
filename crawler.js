var cheerio = require('cheerio');
var http = require('follow-redirects').https;
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/indeed');
var request = require('request');
const ProxyList = require('free-proxy');
const proxyList = new ProxyList();

var Schema = mongoose.Schema;
var jobSchema = new Schema({
    url: String,
    slug: String,
    title: String,
    company: String,
    location: String,
    text: String,
    html: String,
    createTime: Date,
    isArchived: Boolean,
});
var Job = mongoose.model('Job', jobSchema);

var baseUrl = 'https://ca.indeed.com';
// Connection URL
const dburl = 'mongodb://localhost:27017';
// Database Name
// Create a new MongoClient
const client = new MongoClient(dburl, {useNewUrlParser: true});

//config
var startPage = 0;
var endPage = 10;


 var proxy = 'http://100.24.216.83:80';
  //var proxy = 'http://183.220.145.3:80';
    // var proxy = 'https://176.9.75.42:8080';


Job.find().count((err, result) => {
  console.log('total jobs:'+ result);
});

// //can get a new proxy if need recaptcha
proxyList.get()
  .then(function (proxies) {
      console.log(proxies);
  })
  .catch(function (error) {
      console.log(error);
  });


//do work
// Use connect method to connect to the Server
client.connect(function (err) {
    assert.equal(null, err);
    console.log("Connected successfully to db");
    const db = client.db('indeed');

    for (var i = startPage; i <= endPage; i++) {
        // Get Data From the website
        if (i == 0) {
            var url = baseUrl + '/jobs?q=Web+Developer&sort=date';
        } else {
            var url = baseUrl + '/jobs?q=Web+Developer&sort=date&start=' + i * 10;
        }
 

        request({
            'url': url,
            'method': "GET",
            'proxy': proxy
        }, function (error, response, body) {
            console.log('list: '+url);
 
          console.log(body);
            if (!error && response.statusCode == 200) {
                let dateArr = getList(body);
                console.log(dateArr);
            }
        })


    }

    // client.close();

});

//end work


//open single job detail page
function openDetails(url, slug) {
    request({
        'url': baseUrl + url,
        'method': "GET",
        'proxy': proxy
    }, function (error, response, body) {
        let dateArr = getDetails(body, url, slug);
    })
}

//get urls from list page
function getList(str) {
    var $ = cheerio.load(str);

    var arr = $(".jobsearch-SerpJobCard")
    var dataTemp = [];
    arr.each(function (k, v) {
        var src = $(v).find('h2.title a').attr('href');
        var slug = $(v).attr('data-jk');
        dataTemp.push(src);
        openDetails(src, slug);
    })
    return dataTemp;
}

//get single job data from detail page
function getDetails(str, url, slug) {
  if(str){
    try {
        var $ = cheerio.load(str);

        var title = $('.jobsearch-JobInfoHeader-title').first().text();
        var company = $('.jobsearch-InlineCompanyRating .icl-u-xs-mr--xs').first().text();
        var location = $('.jobsearch-InlineCompanyRating div').last().text();
        var content = $(".jobsearch-ViewJobLayout-jobDisplay").first();

        var text = content.text();
        var html = content.html();

        console.log(slug);
        console.log(title);
        console.log(company);
        console.log(location);

        Job.findOne({slug: slug}).count((err, result) => {
            if (err) return console.error(err);
            if (result == 0) {
                var job = new Job({
                    url: url,
                    slug: slug,
                    title: title,
                    company: company,
                    location: location,
                    text: text,
                    html: html,
                    createTime: Date.now(),
                });
                job.save((err, doc) => {
                  console.log('new -- saved: '+ slug);
                    if (err) return console.error(err);
                });

            } else{
              console.log('exists -- ignored: '+ slug);
            }
        });
    } catch (error) {
        console.error(error);

    }
  }
    


}

