Ineed.ca crawler

#make sure you have  Node 6.9.0 or higher, together with NPM 3 or higher.
Node -v
NPM -v

 


#Install
npm install

#Run the crawler, update proxy ip if ask for recaptcha
node crawler.js


#Run the server
node api/api.js

APIs:
/api/jobs get|post
/api/jobs/:job_id get|patch|put|delete
